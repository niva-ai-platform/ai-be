# ai-platform-testbackend



## Developer setup

. Install npm and nodejs
. Clone repo
. Initialise a nodejs environment and start server.

```
npm install
npm start
```

. This will default to port 3000 on localhost.


### Quickstart

#### Grant applications

First you have to create a grant application.

. `GET http://localhost:3000/v1/application/schemes`

returns a list of grant application schemes  eg. ["aes", "env"].

. Use one of these codes to start an application under that grant scheme
`GET http://localhost:3000/v1/application/start/<schemeid>`

returns the application id.  At some point this should be moved to a POST request.


. `GET http://localhost:3000/v1/application/get/<appid>`
returns a specific application information.

#### For testing purposes

There are 3 applications (pre-started) in the Database code:

- '202206-305001', author is 'me', and the grant scheme is 'aes'
- '202207-015002', author is 'me', and the grant scheme is 'aes'),
- '202207-015003', author is 'me', 'and the grant scheme is 'tis');



#### Storage / File upload
Once a grant application id is available, evidence supporting that application can be uploaded.

Upload a photo
. `POST http://localhost:3000/v1/evidence/upload/<appid>`

Body should be the photo.
Response will be:
```
{
  "success": true,
  "file": file-url-on-the-server,
  "appid": appid,
  "imgid": imgid
}
```

Storage will need to allow access to download the images (for the frontend to display)


. `GET http://localhost:3000/v1/storage/<appid>/<img-id>`

Response will be:
 Body should be the photo.


#### Evidence management

With the image id you can tag a photo with an annotation

. `PUT http://localhost:3000/v1/evidence/tag/<appid>/<img-id>/<tagname>`

Response will be all the tags:
```
{
  success: true,
  tags: [annotation-name-1, annotation-name-2]
}
```

With the image id you can remove a tag (annotation) on a photo

. `PUT http://localhost:3000/v1/evidence/untag/<appid>/<img-id>/<tagname>`

Response will be all the tags:
```
{
  success: true,
  tags: [annotation-name-1, annotation-name-2]
}
```


#### ML pipeline: pipeline: Image inference

. `POST http://localhost:3000/v1/models/<modelname>/infer`

Input needs to be a JSON body with the following:

```
{
  "session": "session-identifier",
  "auth": "<jwt token>",
  "farmid": "some-identifier",
  "grantid": "application-id",
  "pipeline": "ML pipeline name",
  "images": [
    "URL of image"
  ]
}
```

#### ToDo

1. NOTE: there is currently no way to list the set of available ML models.

2. Missing calls to register ML models and their containers.

3. Removing images?

4. Missing calls to manage grant applications.
