/*
 * (c) 2022, SETU. Produced for the 3AI project
 */
import mysql from 'mysql2/promise';
import { getFileNameFromUrl, make_identifier } from './utils';

const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME || 'testing',
});

pool.on('acquire', (connection) => {
  console.log('Got connection %d from pool', connection.threadId);
})
pool.on('release', (connection) => {
  console.log('Releasing connection %d back to pool', connection.threadId);
})

const querySql = async (query: string, params: any[]) => {
  const connection = await pool.getConnection();

  if (typeof params !== 'undefined'){
    const [rows] = await connection.query(query, params);
    connection.release();
    return rows;
  } else {
    const [rows] = await connection.query(query);
    connection.release();
    return rows;
  }
};

////////////////QUERIES////////////////////

//
// Returns a promise containg the schemes
//
function get_schemes() {
  const query_str = "SELECT code AS schemeid, name AS scheme FROM grantschemes";
  return querySql(query_str, []);
}

//
// Returns a promise containg the schemes whose types match
//
function get_scheme(type: string) {
  const query_str = "SELECT code as schemeid, name AS scheme FROM grantschemes WHERE code = ?";
  return querySql(query_str, [type]);
}

//
// Makes an application in the DB.
//
// Returns a promise containing the application or an error message
//
async function make_application(author: string, schemeid: string) {
  const insert_str = "INSERT INTO applications(appid,author,schemeid) VALUES (?,?,?)";
  const update_str = "UPDATE applications SET appid=? WHERE id=?";

  let id: string;

  try {
    const r: any = await querySql(insert_str, ['empty', author, schemeid]);
    id = make_identifier(r.insertId);
    console.log("Id:" + id);
    const s: any = await querySql(update_str, [id, r.insertId]);
    return {
      "applicationid": id,
      //"farmid": "IE300300200",
      "author": author,
      "schemeid": schemeid
    };
  } catch (err) {
    console.log('Error creating application', err);
  }

  // return querySql(insert_str, ['empty', author, schemeid])
  //   .then((r: any) => {
  //     id = make_identifier(r.insertId);
  //     console.log("Id:" + id);
  //     return (querySql(update_str, [id, r.insertId]));
  //   }).then(s => {
  //     // console.log("Updating");
  //     // console.log("Id:" + id);
  //     return Promise.resolve({
  //       "applicationid": id,
  //       //"farmid": "IE300300200",
  //       "author": author,
  //       "schemeid": schemeid
  //     });
  //   }).catch((err) => {
  //     Promise.reject("Error creating application");
  //   });
}

//
// Returns a promise containing the application whose id matches
//
function get_application(id: string) {
  const query_str = "SELECT id AS _id, schemeid as schemeid, author AS author, appid AS applicationid, created_at AS started FROM applications WHERE appid = ?";
  return querySql(query_str, [id]);
}


//
// Return a list of known annotations
//
// [ {tag:"review", description:"Manual review requested"}, ...]
//
function get_all_annotations(lang="en") {
  const query_str = "SELECT id AS annotation, displayname AS description FROM annotations WHERE lang=?";
  return querySql(query_str, [lang]);
}
  
//
// Save evidence record to DB
//
const evidence_image_save = (appid: string, author: string, fileName: string) => {
  const query_str = `INSERT INTO photoevidence (appid, author, filename) VALUES ('${appid}', '${author}', '${fileName}')`
  return querySql(query_str, []);
}

//
// Retrieve image from DB
//
const get_evidence_image = (appid: string, fileName: string) => {
  const query_str = `SELECT * FROM photoevidence WHERE appid = '${appid}' AND filename = '${fileName}'`;
  return querySql(query_str, []);
}

//
// Check if a specified annotation is in a list of known annotations
//
// [ {tag:"review", description:"Manual review requested"}, ...]
//
function check_annotation(annotation: string, lang="en") {
  const query_str = "SELECT id AS annotation, displayname AS description FROM annotations WHERE id=?";
  return querySql(query_str, [annotation]);
}

//
// Tag a photo with a the specified annotation
//
function evidence_image_tag(imgid: string, annotation: string, author: string) {
  const query_str = "INSERT INTO photoannotations (imgid, annotation, author) VALUES ((SELECT id from photoevidence p WHERE p.id=?), ?, ?)";
  return querySql(query_str, [parseInt(imgid,10), annotation, author]);
}

//
// Tag a photo with a the specified annotation
//
function evidence_multiple_image_tag(tagEntries: any[]) {
  let query_str = "INSERT INTO image_tags (img_id, tag, author) VALUES ";
  let paramsArray: any[] = [];

  tagEntries.forEach((tagObj: any, i: number) => {
    query_str = query_str + (i === 0 ? '' : ', ') + '(?,?,?)';
    const params = [tagObj.imgid, tagObj.tag, tagObj.author];
    paramsArray = [...paramsArray, ...params];
  });
  return querySql(query_str, paramsArray);
}


//
// Remove a specified annotation from a photo
//
function evidence_image_untag(imgid:string, annotation: string, author: string) {
  const query_str = "DELETE FROM photoannotations WHERE (imgid=? AND annotation=?)";
  return querySql(query_str, [imgid, annotation]);
}

//
// List annotatons on a specified  photo
//
// [ "tagname1", "tagname2", ...]
//
async function evidence_image_list(imgid: string) {
  const query_str = "SELECT annotation FROM photoannotations WHERE (imgid=?)";

  try {
    const annotations: any = await querySql(query_str, [imgid]);
    let r: any[] = [];
      annotations.map((x: any) => {
        r.push(x.annotation);
    });
    return r;
  } catch (err) {
    console.log(err);
  }

  // return querySql(query_str, [imgid])
  //   .then((annotations: any) =>{
  //     let r: any[] = [];
  //     annotations.map((x: any) => {
  //       r.push(x.annotation);
  //   });
  //     return Promise.resolve(r);
  //   });
}


//
// List evidence for a specific application  (minus the tags)
//
// [ {imdid: "imag", author:"me", filename:"202202-05/1001.jpg", uploaded_at:"timestamp"}, ...]
//
//
function get_evidence_v2(appid: string) {
  const query_str = "SELECT e.id AS imgid, e.author AS author, e.filename AS filename, e.created_at AS uploaded_at FROM photoevidence e WHERE (e.appid=?)";
  return querySql(query_str, [appid]);
}

const get_evidence_ids = (filenames: string[]) => {
  let query_str = "SELECT id FROM photoevidence WHERE (";

  filenames.forEach((filename, i) => {
    query_str = query_str + (i === 0 ? 'filename = ?' : ' OR filename = ?');
    if (i === filenames.length - 1) query_str = query_str + ')';  });

  return querySql(query_str, filenames);
}

const get_evidence_filenames = (ids: string[]) => {
  let  query_str = "Select filename FROM photoevidence WHERE (";

  ids.forEach((id, i) => {
    query_str = query_str + (i === 0 ? 'id = ?' : ' OR id = ?');
    if (i === ids.length - 1) query_str = query_str + ')';
  });
  return querySql(query_str, ids);
}

//
// Save ai results to db
//
const ai_results_save = (resultsArray: Object[]) => {
  let query_str = `INSERT INTO ai_results (filename, passed, confidence, ai_module_name, ai_model, results) VALUES `;
  let paramsArray: any[] = [];

  resultsArray.forEach((obj: any, i) => {
    const fileName = getFileNameFromUrl(obj.metadata.inputFileUrl);
    query_str = query_str + (i === 0 ? '' : ', ') + '(?,?,?,?,?,?)';
    const params = [fileName, obj.results.targetDetected, obj.results.maxConfidence, obj.metadata.serviceName, obj.metadata.modelName, JSON.stringify(obj.results.detections)];
    paramsArray = [...paramsArray, ...params];
  });
  return querySql(query_str, paramsArray);
}

//
// Retrieve ai results for given file from db
//
const ai_get_results_by_file = (fileName: string) => {
  const query_str = `SELECT * FROM ai_results WHERE filename = ?`
  return querySql(query_str, [fileName]);
}

//
// Retrieve all ai results
//
const ai_get_results = () => {
  const query_str = `SELECT * from ai_results`;
  return querySql(query_str, []);
}

//
// Retrieve all ai results by model
//
// TODO

//
// Retrieve count of all ai results
//
const ai_get_results_count = () => {
  const query_str = `SELECT id FROM ai_results`;
  return querySql(query_str, []);
}

//
// Retrieve count of all ai results by model
//
const ai_get_results_count_by_model = (model: string) => {
  const query_str = `SELECT id FROM ai_results WHERE ai_module_name = ?`;
  return querySql(query_str, [model]);
}

//
// Retrieve count of all ai results where passed = true
//
const ai_get_results_passed_count = () => {
  const query_str = `SELECT id FROM ai_results WHERE passed = 1`;
  return querySql(query_str, []);
}

//
// Retrieve count of all ai results where passed = true by model
//
const ai_get_results_passed_count_by_model = (model: string) => {
  const query_str = `SELECT id FROM ai_results WHERE passed = 1 AND ai_module_name = ?`;
  return querySql(query_str, [model]);
}

//
// Retrieve count of all ai results where passed = false
//
const ai_get_results_rejected_count = () => {
  const query_str = `SELECT id FROM ai_results WHERE passed = 0`;
  return querySql(query_str, []);
}

//
// Retrieve count of all ai results where passed = false by model
//
const ai_get_results_rejected_count_by_model = (model: string) => {
  const query_str = `SELECT id FROM ai_results WHERE passed = 0 AND ai_module_name = ?`;
  return querySql(query_str, [model]);
}

//
// Retrieve count of all ai results where passed = false
//
const ai_get_results_most_recent_date = () => {
  const query_str = `SELECT MAX(created_at) AS most_recent FROM ai_results`;
  return querySql(query_str, []);
}

//
// Retrieve count of all ai results where passed = false by model
//
const ai_get_results_most_recent_date_by_model = (model: string) => {
  const query_str = `SELECT created_at AS most_recent FROM ai_results WHERE ai_module_name = ? ORDER BY created_at DESC LIMIT 1`;
  return querySql(query_str, [model]);
}

//
// Save module to db
//
const ai_module_save = (name: string, url: string, stage: string, status: string, updateImage: boolean, authenticated: boolean) => {
  const query_str = `INSERT INTO ai_module_registry (name, url, stage, status, update_image, authenticated) VALUES (?,?,?,?,?,?)`
  return querySql(query_str, [name, url, stage, status, updateImage, authenticated]);
}

//
// Retrieve ai modules from db
//
const get_ai_modules = () => {
  const query_str = `SELECT * FROM ai_module_registry`
  return querySql(query_str, []);
}
  
export = {
    // getSqlConnection : getSqlConnection,
    querySql : querySql,
    get_schemes,
    get_scheme,
    make_application,
    get_application,
    get_all_annotations,
    evidence_image_save,
    get_evidence_image,
    check_annotation,
    evidence_image_tag,
    evidence_multiple_image_tag,
    evidence_image_untag,
    evidence_image_list,
    get_evidence_v2,
    get_evidence_ids,
    get_evidence_filenames,
    ai_results_save,
    ai_get_results_by_file,
    ai_get_results,
    ai_get_results_count,
    ai_get_results_count_by_model,
    ai_get_results_passed_count,
    ai_get_results_passed_count_by_model,
    ai_get_results_rejected_count,
    ai_get_results_rejected_count_by_model,
    ai_get_results_most_recent_date,
    ai_get_results_most_recent_date_by_model,
    ai_module_save,
    get_ai_modules,
};
