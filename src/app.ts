/*
 * (c) 2022, SETU. Produced for the 3AI project
 */
import express, { Application } from 'express';
import session from 'express-session';
import 'dotenv/config';
import Keycloak from 'keycloak-connect';

const app: Application = express();
const PORT = process.env.PORT || 3000;

// Logging
//app.use(logger('dev'));

// Create a session-store to be used by both the express-session
// middleware and the keycloak middleware.
export const memoryStore = new session.MemoryStore();

// Provide the session store to the Keycloak so that sessions
// can be invalidated from the Keycloak console callback.
//
// Additional configuration is read from keycloak.json file
// installed from the Keycloak web console.
export const keycloak = new Keycloak({
  store: memoryStore,
});

app.use(session({
  secret: 'someSecret',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));

// Use standard middleware
app.use(express.json());
app.use(express.urlencoded({ extended:false }));

// Routes
import authRouter from './routes/auth';
import { appRouter } from './routes/application';
import { evidenceRouter } from './routes/evidence';
import { resultsRouter } from './routes/results';
import { registryRouter } from './routes/registry';
import { taggingRouter } from './routes/tagging';

app.use('/api/v1/auth', authRouter);
app.use('/api/v1/application', appRouter);
app.use('/api/v1/evidence', evidenceRouter);
app.use('/api/v1/results', resultsRouter);
app.use('/api/v1/registry', registryRouter);
app.use('/api/v1/tagging', taggingRouter);


app.listen(PORT, () => {
  console.info(`App listening on port ${PORT}`);
});
