/*
 * (c) 2022, SETU. Produced for the 3AI project
 */
import express from 'express';
// import { Promise } from 'bluebird';

// MySql DB access
import db from '../db';
import { keycloak } from '../app';
import { decodeAuthToken } from '../utils';

export const evidenceRouter = express.Router();

//
// Various constants
//
var counters = [
  {
    id: "storage",
    name: "Storage driver",
    counter: 10002
  }
];

const MAX_EVIDENCE = 5;
const EVIDENCE_LOCATION = "http://storage.niva.com/";
const NINETY_DAYS =   3600000*24*90;

evidenceRouter.use(keycloak.middleware({
  logout: '/logout',
  admin: '/'
}));

//
// Routes
//
evidenceRouter.get('/upload/:appid', (req, res, next) => {
  // Response contains
  const r = {
    "success": false,
    "reason": "Wrong endpoint you need to use a POST request",
    "file": '',
    "appid": '',
    "imgid": ''
  };

  // Return response
  res.setHeader('Content-Type', 'application/json');
  res.json(r);
});


evidenceRouter.post('/upload/:appid/:fname', 
  keycloak.protect('realm:upload-image'),
  async (req, res, next) => {
    //res.send("You have hit the evidence/upload endpoint.");

    // Recover app id and filename
    const appid = req.params.appid;
    const author = decodeAuthToken(req.headers.authorization).sub;
    const fname = req.params.fname;

    try {
      const response: any = await db.evidence_image_save(appid, author, fname);
      console.log("R: ", response.affectedRows === 1 ? `Record saved as id: ${response.insertId}` : 'Record not saved');
      // Return response
      res.status(200).json(response);
    } catch (err) {
      console.error("R: Got error:" + err);
      if (err instanceof Error){
        res.status(200).json({ success: false, message:"Invalid request"});
      } else {
        res.status(200).json({ success: false, message:err});
      }
    }
  }
);


// //
// // Get all evidence related to a specific application
// //
// // {
// // success: true,
// // data: [
// //  {imdid:"imag", author:"user", filename:"202202-05/1001.jpg", uploaded_at:"timestamp", tags:[ "tagname1", "tagname2", ...]},
// //  ...]
// // }
// //
// evidenceRouter.get('/get/appid/:appid', (req, res,next) => {
//   console.log("You have hit the evidence get endpoint.");
//   res.setHeader('Content-Type', 'application/json');

//   // Recover app id
//   var appid = req.params.appid;

//   let r={
//     success: true,
//     data: [] as any[]
//   };
//   // Query for a list of annotations
//   Promise.resolve().then(function () {
//     return db.get_evidence_v2(appid);
//   }).then((evidence: any[]) => {
//     console.log("R: " + evidence.length + " items returned");
//     r.data = evidence;

//     let subs: any[] = [];
//     r.data.forEach((x) => {
//       console.log("R: " + x.imgid + " requested");
//       subs.push(db.evidence_image_list(x.imgid));
//     });

//     return Promise.all(subs).thenReturn(subs);
//   }).then((subs: any[]) => {
//     console.log("Tags:" + subs.length);
//     for (var i=0;i < subs.length;++i) {
//       let v = subs[i].value();
//       r.data[i]['tags']=v;
//     }
//     // Return response
//     res.status(200).json(r);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });


//
// Get image id from filename
//
evidenceRouter.get('/get/ids/:filenames', async (req, res,next) => {
  // console.log("You have hit the evidence get endpoint.");
  res.setHeader('Content-Type', 'application/json');

  const filenames = req.params.filenames.split(',');

  let r={
    success: true,
    data: [] as any[]
  };

  try {
    const ids: any = await db.get_evidence_ids(filenames);
    console.log("R: " + ids.length + " items returned");
    r.data = ids;
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});


//
// Get image filenames from ids
//
evidenceRouter.get('/get/filenames/:ids', async (req, res,next) => {
  // console.log("You have hit the evidence get endpoint.");
  res.setHeader('Content-Type', 'application/json');
  const ids = req.params.ids.split(',');

  let r = {
    success: true,
    data: [] as any[]
  };

  try {
    const filenames: any = await db.get_evidence_filenames(ids);
    console.log("R: " + filenames.length + " items returned");
    r.data = filenames;
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});


// evidenceRouter.post('/ai-results', (req, res, next) => {
//   // Query for saving results data
//   Promise.resolve().then(() => {
//     return ai_results_save(req.body);
//   }).then((results: any) => {
//     console.log("R: ", results.affectedRows > 0 
//       ? results.affectedRows === 1 
//         ? `1 record saved with id: ${results.insertId}` 
//         : `${results.affectedRows} records saved starting with id: ${results.insertId}` 
//           : 'No record saved');
//     // Return response
//     res.status(200).json(results);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });

// evidenceRouter.get('/ai-results/:filename', (req, res, next) => {
//   const fileName = req.params.filename;

//    // Query for a list ai results
//    Promise.resolve().then(function () {
//     return get_ai_results(fileName);
//   }).then((results: any[]) => {
//     console.log("R: " + results.length + " items returned");
//     const r = {
//       success: true,
//       data: results
//     };
//     // Return response
//     res.status(200).json(r);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });

// evidenceRouter.post('/module-registry', (req, res, next) => {
//   const module = req.body;
//   console.log(module);
//   // Query for saving results data
//   Promise.resolve().then(() => {
//     return ai_module_save(module.name, module.url, module.stage, module.status, module.hasOwnProperty('updateImage') ? module.updateImage : null , module.authenticated);
//   }).then((results: any) => {
//     console.log("R: ", results.affectedRows === 1 ? `Record saved as id: ${results.insertId}` : 'Record not saved');
//     // Return response
//     res.status(200).json(results);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });

// evidenceRouter.get('/module-registry', (req, res, next) => {
//    // Query for a list ai results
//    Promise.resolve().then(function () {
//     return get_ai_modules();
//   }).then((results: any[]) => {
//     console.log("R: " + results.length + " items returned");
//     const r = {
//       success: true,
//       data: results
//     };
//     // Return response
//     res.status(200).json(r);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });

// //
// // Return a list of known annotations
// //
// // [ {tag:"review", description:"Manual review requested"}, ...]
// //
// function get_all_annotations(lang="en") {
//   const query_str = "SELECT id AS annotation, displayname AS description FROM annotations WHERE lang=?";
//   return db.querySql(query_str, [lang]);
// }

// //
// // Save evidence record to DB
// //
// const evidence_image_save = (appid: string, author: string, fileName: string) => {
//   const query_str = `INSERT INTO photoevidence (appid, author, filename) VALUES ('${appid}', '${author}', '${fileName}')`
//   return db.querySql(query_str, []);
// }

// //
// // Retrieve image from DB
// //
// const get_evidence_image = (appid: string, fileName: string) => {
//   const query_str = `SELECT * FROM photoevidence WHERE appid = '${appid}' AND filename = '${fileName}'`;
//   return db.querySql(query_str, []);
// }

// //
// // Check if a specified annotation is in a list of known annotations
// //
// // [ {tag:"review", description:"Manual review requested"}, ...]
// //
// function check_annotation(annotation: string, lang="en") {
//   const query_str = "SELECT id AS annotation, displayname AS description FROM annotations WHERE id=?";
//   return db.querySql(query_str, [annotation]);
// }

// //
// // Tag a photo with a the specified annotation
// //
// function evidence_image_tag(imgid:string, annotation: string, author: string) {
//   const query_str = "INSERT INTO photoannotations (imgid, annotation, author) VALUES ((SELECT id from photoevidence p WHERE p.id=?), ?, ?)";
//   return db.querySql(query_str, [parseInt(imgid,10), annotation, author]);
// }

// //
// // Remove a specified annotation from a photo
// //
// function evidence_image_untag(imgid:string, annotation: string, author: string) {
//   const query_str = "DELETE FROM photoannotations WHERE (imgid=? AND annotation=?)";
//   return db.querySql(query_str, [imgid, annotation]);
// }

// //
// // List annotatons on a specified  photo
// //
// // [ "tagname1", "tagname2", ...]
// //
// function evidence_image_list(imgid: string) {
//   const query_str = "SELECT annotation FROM photoannotations WHERE (imgid=?)";
//   return db.querySql(query_str, [imgid])
//     .then((annotations: any []) =>{
//       let r: any[] = [];
//       annotations.map((x) => {
//         r.push(x.annotation);
//       });
//       return Promise.resolve(r);
//     });
// }


// //
// // List evidence for a specific application  (minus the tags)
// //
// // [ {imdid: "imag", author:"me", filename:"202202-05/1001.jpg", uploaded_at:"timestamp"}, ...]
// //
// //
// function get_evidence_v2(appid: string) {
//   const query_str = "SELECT e.id AS imgid, e.author AS author, e.filename AS filename, e.created_at AS uploaded_at FROM photoevidence e WHERE (e.appid=?)";
//   return db.querySql(query_str, [appid]);
// }

// //
// // Save ai results to db
// //
// const ai_results_save = (resultsArray: Object[]) => {
//   const fileNameRegEx = /image-fs\/files\/(.*)\/(.*)/;
//   let query_str = `INSERT INTO ai_results (filename, passed, confidence, ai_module_name, ai_model, results) VALUES `;
//   let paramsArray: any[] = [];

//   resultsArray.forEach((obj: any, i) => {
//     const fileName = fileNameRegEx.exec(obj.metadata.inputFileUrl)[2];
//     query_str = query_str + (i === 0 ? '' : ', ') + '(?,?,?,?,?,?)';
//     const params = [fileName, obj.results.targetDetected, obj.results.maxConfidence, obj.metadata.modelName, obj.metadata.modelType, JSON.stringify(obj.results.detections)];
//     paramsArray = [...paramsArray, ...params];
//   });
//   return db.querySql(query_str, paramsArray);
// }

// //
// // Retrieve ai results for given file from db
// //
// const get_ai_results = (fileName: string) => {
//   const query_str = `SELECT * FROM ai_results WHERE filename = ?`
//   return db.querySql(query_str, [fileName]);
// }

// //
// // Save module to db
// //
// const ai_module_save = (name: string, url: string, stage: string, status: string, updateImage: boolean, authenticated: boolean) => {
//   const query_str = `INSERT INTO ai_module_registry (name, url, stage, status, update_image, authenticated) VALUES (?,?,?,?,?,?)`
//   return db.querySql(query_str, [name, url, stage, status, updateImage, authenticated]);
// }

// //
// // Retrieve ai modules from db
// //
// const get_ai_modules = () => {
//   const query_str = `SELECT * FROM ai_module_registry`
//   return db.querySql(query_str, []);
// }


// //
// //  Code below will be deprecated
// //

// //LEFT JOIN photoannotations a ON e.id = a.imgid

// // Returns a random amount of evidence
// function get_evidence(appid: string) {
//     var amount = Math.floor(Math.random() * MAX_EVIDENCE);
//     var r = new Array();
//     for (var i=0;i < amount;i++) {
//       const fname = EVIDENCE_LOCATION + make_storage_name() + ".jpg";
//       r.push({
//         "file": fname,
//         "appid": appid,
//         "check": to_hex(hashCode(fname)),
//         "timestamp": make_upload_time()
//       });
//     }
//     return r;
// }

// // Compute hash for the string
// function hashCode(str: string) {
//     let hash = 0;
//     for (let i = 0, len = str.length; i < len; i++) {
//         let chr = str.charCodeAt(i);
//         hash = (hash << 5) - hash + chr;
//         hash |= 0; // Convert to 32bit integer
//     }
//     return hash;
// }

// // Convert to hex string
// function to_hex(n: number) {
//   var ret = ((n<0?0x8:0)+((n >> 28) & 0x7)).toString(16) + (n & 0xfffffff).toString(16);
//   while (ret.length < 8) ret = '0'+ret;
//   return ret;
// }


// // Eg. '20121101110033'
// function make_storage_name() {
//   // Select storage counter
//   var storage = counters[counters.length-1];

//   const d = new Date().toISOString();
//   var p = d.split('T');
//   var id = p[0].replace(/-/g, '') + storage.counter++;
//   return id;
// };

// //
// // Random upload times
// //
// function make_upload_time() {
//   var time = new Date("01/01/2022");
//   var days = Math.floor(Math.random() * 90);

//   return time.setDate(time.getDate() + days);
// };

// function get_tag(tagname: string) {
//   const id = Math.floor(Math.random() * 90);

//   var urn = "nivatag:dafm/";
//   if (tagname === "review") {
//     urn = urn + "pii/" + tagname;
//   } else {
//     urn = urn + tagname;
//   }

//   return {
//     name: tagname,
//     urn: urn,
//     id:  id
//   }
// };

// export const evidence = get_evidence;
