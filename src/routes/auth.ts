/*
 * (c) 2022, SETU. Produced for the 3AI project
 */

import express from 'express';
const authRouter = express.Router();


authRouter.get('/', (req, res, next) => {
  res.send("You have hit the auth endpoint.");
});

authRouter.get('/login', (req, res, next) => {
  res.send("You have hit the auth/login endpoint.");
});

authRouter.get('/logout', (req, res, next) => {
  res.send("You have hit the auth/logout endpoint.");
});

export = authRouter;
