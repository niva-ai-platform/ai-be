/*
 * (c) 2022, SETU. Produced for the 3AI project
 */
import express from 'express';
// import { Promise } from 'bluebird';

// MySql DB access
import db from '../db';
import { keycloak } from '../app';
import { decodeAuthToken } from '../utils';

export const registryRouter = express.Router();

registryRouter.use(keycloak.middleware({
  logout: '/logout',
  admin: '/'
}));

//
// Routes
//
registryRouter.post('/module-registry', async (req, res, next) => {
  const module = req.body;
  console.log(module);

  try {
    const results: any = 
      await db.ai_module_save(
        module.name, 
        module.url, 
        module.stage, 
        module.status, 
        module.hasOwnProperty('updateImage') ? module.updateImage : null , 
        module.authenticated
      );
    console.log("R: ", results.affectedRows === 1 ? `Record saved as id: ${results.insertId}` : 'Record not saved');
    // Return response
    res.status(200).json(results);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
  // // Query for saving results data
  // Promise.resolve().then(() => {
  //   return db.ai_module_save(module.name, module.url, module.stage, module.status, module.hasOwnProperty('updateImage') ? module.updateImage : null , module.authenticated);
  // }).then((results: any) => {
  //   console.log("R: ", results.affectedRows === 1 ? `Record saved as id: ${results.insertId}` : 'Record not saved');
  //   // Return response
  //   res.status(200).json(results);
  // }).catch((err: Error) => {
  //   console.error("R: Got error:" + err);
  //   if (err instanceof Error){
  //     res.status(200).json({ success: false, message:"Invalid request"});
  //   } else {
  //     res.status(200).json({ success: false, message:err});
  //   }
  // });
});

registryRouter.get('/module-registry', async (req, res, next) => {

  try {
    const results: any = await db.get_ai_modules();
    console.log("R: " + results.length + " items returned");
    const r = {
      success: true,
      data: results
    };
    // Return response
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }

  //  // Query for a list ai results
  //  Promise.resolve().then(function () {
  //   return db.get_ai_modules();
  // }).then((results: any[]) => {
  //   console.log("R: " + results.length + " items returned");
  //   const r = {
  //     success: true,
  //     data: results
  //   };
  //   // Return response
  //   res.status(200).json(r);
  // }).catch((err: Error) => {
  //   console.error("R: Got error:" + err);
  //   if (err instanceof Error){
  //     res.status(200).json({ success: false, message:"Invalid request"});
  //   } else {
  //     res.status(200).json({ success: false, message:err});
  //   }
  // });
});
