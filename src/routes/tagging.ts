/*
 * (c) 2022, SETU. Produced for the 3AI project
 */
import express from 'express';
// import { Promise } from 'bluebird';

// MySql DB access
import db from '../db';
import { keycloak } from '../app';
import { decodeAuthToken } from '../utils';

export const taggingRouter = express.Router();

taggingRouter.use(keycloak.middleware({
  logout: '/logout',
  admin: '/'
}));

//
// Routes
//
//

// //
// // Return a list of known tags
// //
// // {
// // success: true,
// // data: [ {tag:"review", description:"Manual review requested"}, ...]
// // }
// //
// taggingRouter.get('/tags', (req, res, next) => {
//   console.log("You have hit the evidence tags endpoint.");
//   res.setHeader('Content-Type', 'application/json');

//   // TODO fix language handling
//   //const lang = req.headers['Accept-Lang'] || "en";

//   // Query for a list of annotations
//   Promise.resolve().then(function () {
//     return db.get_all_annotations();
//   }).then((annotations: any[]) => {
//     console.log("R: " + annotations.length + " annotations returned");
//     // Return response
//     res.status(200).json(annotations);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });


// //
// // Add an annotation to a specific photo
// //
// // {
// // success: true,
// // data: [ "tagname1", "tagname2", ...]
// // }
// //
// taggingRouter.put('/tag/:imgid/:tag', (req, res, next) => {
//   // List of annotations to add
//   console.log("You have hit the evidence tag endpoint.");
//   res.setHeader('Content-Type', 'application/json');

//   const imgid = req.params.imgid;
//   const annotation = req.params.tag;
//   // TODO fix the extraction from the JWT.
//   const author = req.body.author || "user";

//   // Add an annotation
//   Promise.resolve().then(function () {
//     console.log("I: imgid="+imgid+", annotation=" + annotation + ", author=" + author);
//     return db.evidence_image_tag(imgid, annotation, author);
//   }).then((result: string) => {
//     console.log("I: " + result );
//     // TODO check return value
//     // Query for a list of annotations
//     return db.evidence_image_list(imgid);
//   }).then((annotations: any[]) => {
//     console.log("R: " + annotations.length + " annotations returned");
//     const r = {
//       success: true,
//       data: annotations
//     };
//     // Return response
//     res.status(200).json(annotations);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });

// //
// // Remove an annotation from a specific photo
// //
// // {
// // success: true,
// // data: [ "tagname1", "tagname2", ...]
// // }
// //
// taggingRouter.put('/untag/:imgid/:tag', (req, res, next) => {
//   // List of annotations to remove
//   console.log("You have hit the evidence untag endpoint.");
//   res.setHeader('Content-Type', 'application/json');

//   // Recover img id
//   const imgid = req.params.imgid;
//   const annotation = req.params.tag;
//   // TODO fix the extraction from the JWT.
//   const author = req.body.author || "user";

//   // Add an annotation
//   Promise.resolve().then(function () {
//     return db.evidence_image_untag(imgid, annotation, author);
//   }).then((result: any) => {
//     // TODO check return value
//     // Query for a list of annotations
//     return db.evidence_image_list(imgid);
//   }).then((annotations: any[]) => {
//     console.log("R: " + annotations.length + " annotations returned");
//     const r = {
//       success: true,
//       data: annotations
//     };
//     // Return response
//     res.status(200).json(annotations);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });

// //
// // Get all annotations related to a specific image
// //
// // {
// // success: true,
// // data: [ "tagname1", "tagname2", ...]
// // }
// //
// taggingRouter.get('/annotations/:imgid', (req, res, next) => {
//   console.log("You have hit the evidence annotations get endpoint.");
//   res.setHeader('Content-Type', 'application/json');

//   // Recover app id
//   var imgid = req.params.imgid;

//   // Query for a list of annotations
//   Promise.resolve().then(function () {
//     return db.evidence_image_list(imgid);
//   }).then((annotations: any[]) => {
//     console.log("R: " + annotations.length + " items returned");
//     const r={
//       success: true,
//       data: annotations
//     };
//     // Return response
//     res.status(200).json(r);
//   }).catch((err: Error) => {
//     console.error("R: Got error:" + err);
//     if (err instanceof Error){
//       res.status(200).json({ success: false, message:"Invalid request"});
//     } else {
//       res.status(200).json({ success: false, message:err});
//     }
//   });
// });
