/*
 * (c) 2022, SETU. Produced for the 3AI project
 */
import express, { Request, Response, NextFunction } from 'express';
// import { Promise } from 'bluebird';

// MySql DB access
import db from '../db';
import { keycloak } from '../app';
import { decodeAuthToken, getFileNameFromUrl } from '../utils';
// import {  aiResultsCallback } from './route';

export const resultsRouter = express.Router();

resultsRouter.use(keycloak.middleware({
  logout: '/logout',
  admin: '/'
}));

//
// Routes
//
// TODO: Add keycloak middleware
resultsRouter.post('/ai-results', async (req: Request, res: Response, next: NextFunction) => {
  console.log('You\'ve hit the Save AI Results endpoint', req.body);
  const author = decodeAuthToken(req.headers.authorization).sub || 'user';
  const inputFilenames: string[] = [];
  const tagObjects: any[] = [];
  // TODO: check if image results aready exist. if so, delete and replace.
  req.body.forEach((obj: any, i: number) => {
    // obj.metadata.tags = [`${i}-test-tag-1`, `${i}-test-tag-2`];
    const filename = getFileNameFromUrl(obj.metadata.inputFileUrl);
    if (obj.metadata.tags) obj.metadata.tags.forEach((tag: string) => {
      tagObjects.push({filename: filename, tag: tag, author: author});
    });
    inputFilenames.push(filename);
  });

  try {
    const results: any = await db.ai_results_save(req.body);
    console.log("R: ", results.affectedRows > 0 
        ? results.affectedRows === 1 
          ? `1 AI Results record saved with id: ${results.insertId}` 
          : `${results.affectedRows} AI Results records saved starting with id: ${results.insertId}` 
            : 'No AI Results record saved');

    if (tagObjects.length > 0) {
      const idResults: any = await db.get_evidence_ids(inputFilenames);
      inputFilenames.forEach((filename, i) => {
        tagObjects.forEach((tagObject, j) => {
          if (tagObject.filename === filename) {
            tagObject.imgid = idResults[i].id;
          }
        });
      });
      const tagResults: any = await db.evidence_multiple_image_tag(tagObjects);
      console.log("R: ", tagResults.affectedRows > 0 
      ? tagResults.affectedRows === 1 
        ? `1 Tag added with id: ${tagResults.insertId}` 
        : `${tagResults.affectedRows} Tags added starting with id: ${tagResults.insertId}` 
          : 'No Tags added');

      res.status(200).json(results);
    } else {
      console.log('R: No Tags added');
      res.status(200).json(results);
    }
   } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
   }
});

// Get results associated with specific file
resultsRouter.get('/ai-results-by-file/:filename', async (req, res, next) => {
  const fileName = req.params.filename;

  try {
    const results: any = await db.ai_get_results_by_file(fileName);
    console.log("R: " + results.length + " items returned");
    const r = {
      success: true,
      data: results
    };

    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message: "Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get results count
resultsRouter.get('/ai-results-count/', async (req, res, next) => {
  console.log(`get results count`);

  try {
    const results: any = await db.ai_get_results_count();
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results.length
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get results count by model
resultsRouter.get('/ai-results-count/:model', async (req, res, next) => {
  const model = req.params.model;
  console.log(`get results count by model: ${model}`);

  try {
    const results: any = await db.ai_get_results_count_by_model(model);
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results.length
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get results count where passed = true
resultsRouter.get('/ai-results-passed-count/', async (req, res, next) => {
  console.log(`get results count where pipeline passed`);

  try {
    const results: any = await db.ai_get_results_passed_count();
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results.length
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get results count where passed = true by model
resultsRouter.get('/ai-results-passed-count/:model', async (req, res, next) => {
  const model = req.params.model;
  console.log(`get results count where pipeline passed by model: ${model}`);

  try {
    const results: any = await db.ai_get_results_passed_count_by_model(model);
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results.length
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get results count where passed = false
resultsRouter.get('/ai-results-rejected-count/', async (req, res, next) => {
  console.log(`get results count where pipeline failed`);

  try {
    const results: any = await db.ai_get_results_rejected_count();
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results.length
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get results count where passed = false by model
resultsRouter.get('/ai-results-rejected-count/:model', async (req, res, next) => {
  const model = req.params.model;
  console.log(`get results count where pipeline failed by model: ${model}`);

  try {
    const results: any = await db.ai_get_results_rejected_count_by_model(model);
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results.length
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get most recent results date
resultsRouter.get('/ai-results-most-recent/', async (req, res, next) => {
  console.log(`get most recent results date`);

  try {
    const results: any = await db.ai_get_results_most_recent_date();
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results[0].most_recent
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});

// Get most recent results date by model
resultsRouter.get('/ai-results-most-recent/:model', async (req, res, next) => {
  const model = req.params.model;
  console.log(`get most recent results dateby model: ${model}`);

  try {
    const results: any = await db.ai_get_results_most_recent_date_by_model(model);
    console.log("R: " + results.length + " results records returned");
    const r = {
      success: true,
      data: results[0].most_recent
    };
  
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      res.status(200).json({ success: false, message:"Invalid request"});
    } else {
      res.status(200).json({ success: false, message:err});
    }
  }
});
