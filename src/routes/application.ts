/*
 * (c) 2022, SETU. Produced for the 3AI project
 */

import express from 'express';
export const appRouter = express.Router();

// import { Promise } from 'bluebird';
// MySql DB access
import db from '../db';
import { decodeAuthToken, make_identifier } from '../utils';


// Ability to get evidence
// const evidence = require('./evidence.js');


//
// Return a list of schemes
// Returns
// {
//  success: true,
//  data: [ {schemeid:"aes", scheme:"Agri-Env .."} ]
// }
//
appRouter.get('/schemes', async (req, res, next) => {
  console.log("You have hit the application schemes endpoint.");
  res.setHeader('Content-Type', 'application/json');

  try {
    const schemes: any = await db.get_schemes();
    console.log("R: " + schemes.length + " grant schemes returned");
    const r = {
      success: true,
      data: schemes
    }
    // Return response
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error:" + err);
    if (err instanceof Error){
      //res.setHeader('Content-Type', 'application/json');
      res.status(200).json({ sucesss: false, message:"Invalid request"});
    } else {
      //res.setHeader('Content-Type', 'application/json');
      res.status(200).json({ sucesss: false, message:err});
    }
  }

  // // Query for a list of grant schemes
  // Promise.resolve().then(function () {
  //   return db.get_schemes();
  // }).then((schemes: string[]) => {
  //   console.log("R: " + schemes.length + " grant schemes returned");
  //   const r = {
  //     success: true,
  //     data: schemes
  //   }
  //   // Return response
  //   res.status(200).json(r);
  // }).then(() => next(), err => next(err)
  // ).catch(err => {
  //   console.error("R: Got error:" + err);
  //   if (err instanceof Error){
  //     //res.setHeader('Content-Type', 'application/json');
  //     res.status(200).json({ sucesss: false, message:"Invalid request"});
  //   } else {
  //     //res.setHeader('Content-Type', 'application/json');
  //     res.status(200).json({ sucesss: false, message:err});
  //   }
  // });  //.catch((err) => setImmediate(() => { throw err; }));
});


//
// Create an application under a specific scheme
// Returns
// {
//  success: true,
//  data: [ {schemeid:"aes", applicationid:"202207-175002", author:"user"} ]
// }
//
appRouter.get('/start/:type', async (req, res, next) => {
  console.log("You have hit the application start endpoint.");
  res.setHeader('Content-Type', 'application/json');

  const type = req.params.type;
  const author = decodeAuthToken(req.headers.authorization).sub || 'user';

  try {
    const s: any = await db.get_scheme(type);
    if (s.length == 0){
      // No match
      console.log("No such scheme type");
    } else if (s.length > 1) {
      // Too many matches
      console.log("W: Multiple schemes apply");
    } else {
      const appl: any = await db.make_application(author, s[0].schemeid);
      console.log("R: " + appl.schemeid + ":" + appl.applicationid);
      const r = {
        success: true,
        data: appl
      }
      // Return response
      res.status(200).json(r);
    }
  } catch (err) {
    console.error("R: Got error: " + err);
    if (err instanceof Error){
      res.status(200).json({ sucesss: false, message:"Invalid request"});
    } else {
      res.status(200).json({ sucesss: false, message:err});
    }
  }

  // Promise.resolve().then(function () {
  //   return db.get_scheme(type);
  // }).then((s: any[]) => {
  //   if (s.length == 0){
  //     // No match
  //     return Promise.reject("No such scheme type");
  //   } else if (s.length > 1) {
  //     // Too many matches
  //     console.log("W: Multiple schemes apply");
  //     //return Promise.reject("Multiple schemes apply");
  //   }
  //   // Make application
  //   return db.make_application(author, s[0].schemeid);
  // }).then((appl: {
  //     applicationid: any;
  //     author: any;
  //     schemeid: any;
  //   }) => {
  //     console.log("R: " + appl.schemeid + ":" + appl.applicationid);
  //     const r = {
  //       success: true,
  //       data: appl
  //     }
  //     // Return response
  //     res.status(200).json(r);
  // }).catch(err => {
  //   console.error("R: Got error: " + err);
  //   if (err instanceof Error){
  //     res.status(200).json({ sucesss: false, message:"Invalid request"});
  //   } else {
  //     res.status(200).json({ sucesss: false, message:err});
  //   }
  // });
});


//
// Get a specifc application
// Returns
// {
//  success: true,
//  data: [ {schemeid:"aes", applicationid:"202207-175002", author:"user"} ]
// }
//
appRouter.get('/get/:appid', async (req, res, next) => {
  console.log("You have hit the application get endpoint.");
  res.setHeader('Content-Type', 'application/json');

  // Recover app id
  var appid = req.params.appid;

  try {
    const s: any = await db.get_application(appid);
    if (s.length == 0){
      // No match
      console.log("No such application");
    } else if (s.length > 1) {
      // Too many matches
      console.log("W: Multiple applications match");
    }
    // Apply to the first result
    const r = {
      success: true,
      data: s[0]
    };
    console.log("R: " + s[0].schemeid + ":" + s[0].applicationid);

    // Add supporting evidence  -  TODO fix with TOM
    // This is now a direct call to the evidence endpoint.
    //
    // var ev = evidence[1](appid);
    // r["evidence"]=ev;

    // Return response
    res.status(200).json(r);
  } catch (err) {
    console.error("R: Got error: " + err);
    if (err instanceof Error){
      res.status(200).json({ sucesss: false, message:"Invalid request"});
    } else {
      res.status(200).json({ sucesss: false, message:err});
    }
  }

  // // Query to get application
  // Promise.resolve().then(function () {
  //   return db.get_application(appid);
  // }).then((s: any[]) => {
  //   if (s.length == 0){
  //     // No match
  //     return Promise.reject("No such application");
  //   } else if (s.length > 1) {
  //     // Too many matches
  //     console.log("W: Multiple applications match");
  //     //return Promise.reject("Multiple applications match");
  //   }
  //   // Apply to the first result
  //   const r = {
  //     success: true,
  //     data: s[0]
  //   };
  //   console.log("R: " + s[0].schemeid + ":" + s[0].applicationid);

  //   // Add supporting evidence  -  TODO fix with TOM
  //   // This is now a direct call to the evidence endpoint.
  //   //
  //   // var ev = evidence[1](appid);
  //   // r["evidence"]=ev;

  //   // Return response
  //   res.status(200).json(r);
  // }).catch(err => {
  //   console.error("R: Got error: " + err);
  //   if (err instanceof Error){
  //     res.status(200).json({ sucesss: false, message:"Invalid request"});
  //   } else {
  //     res.status(200).json({ sucesss: false, message:err});
  //   }
  // });
});


/// Here

// Deprecated
// function scheme_select(type) {
//   // Find the matching type
//   let s = counters.filter(x => (x.id == type));
//   //console.log(s);
//
//   if (s.length < 1) {
//     // Pick one at random
//     i = Math.floor(Math.random() * NUM_SCHEMES);
//     s.push(counters[i]);
//     console.log("Missing scheme " + type + "selecting " +s[0].id);
//   }
//   return s[0];
// }

//////////////////////////////////////////////////////////////
//
// Support functions for DB access
//

// //
// // Make an identifier given a number
// // Eg. '201211-045000'
// //
// function make_identifier(counter: number) {

//   const d = new Date().toISOString();
//   //console.log(d);
//   var p = d.split('T');
//   var id = p[0].replace(/-/, '') + counter;
//   return id;
// }

// //
// // Returns a promise containg the schemes
// //
// function get_schemes() {
//   const query_str = "SELECT code AS schemeid, name AS scheme FROM grantschemes";
//   return db.querySql(query_str, []);
// }

// //
// // Returns a promise containg the schemes whose types match
// //
// function get_scheme(type: string) {
//   const query_str = "SELECT code as schemeid, name AS scheme FROM grantschemes WHERE code = ?";
//   return db.querySql(query_str, [type]);
// }

// //
// // Makes an application in the DB.
// //
// // Returns a promise containing the application or an error message
// //
// export function make_application(author: string, schemeid: string) {
//   const insert_str = "INSERT INTO applications(appid,author,schemeid) VALUES (?,?,?)";
//   const update_str = "UPDATE applications SET appid=? WHERE id=?";

//   let id: string;
//   return db.querySql(insert_str, ['empty', author, schemeid])
//     .then((r: any) => {
//       id = make_identifier(r.insertId);
//       console.log("Id:" + id);
//       return (db.querySql(update_str, [id, r.insertId]));
//     }).then(s => {
//       // console.log("Updating");
//       // console.log("Id:" + id);
//       return Promise.resolve({
//         "applicationid": id,
//         //"farmid": "IE300300200",
//         "author": author,
//         "schemeid": schemeid
//       });
//     }).catch((err) => {
//       Promise.reject("Error creating application");
//     });
// }

// //
// // Returns a promise containing the application whose id matches
// //
// function get_application(id: string) {
//   const query_str = "SELECT id AS _id, schemeid as schemeid, author AS author, appid AS applicationid, created_at AS started FROM applications WHERE appid = ?";
//   return db.querySql(query_str, [id]);
// }
