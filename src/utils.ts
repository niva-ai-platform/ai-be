export const make_identifier = (counter: number) => {

  const d = new Date().toISOString();
  //console.log(d);
  var p = d.split('T');
  var id = p[0].replace(/-/, '') + counter;
  return id;
}

export const decodeAuthToken = (authToken: string) => {
  // TODO: cause failure if no Bearer. add try/catch to JSON.parse()
  let token;
  if (authToken.startsWith('Bearer ')) {
    token = authToken.substring(7, authToken.length);
  } else {
    token = authToken;
  }
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const buff = Buffer.from(base64, 'base64');
  const payloadinit = buff.toString('ascii');
  const payload = JSON.parse(payloadinit);
  return payload;
};

export const getFileNameFromUrl = (url: string) => {
  const fileNameRegEx = /image-fs\/files\/(.*)\/(.*)/;
  return fileNameRegEx.exec(url)[2];
};
