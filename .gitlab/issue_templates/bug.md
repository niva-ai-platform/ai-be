# Summary
<place a description of the bug here>

### <Internal / External>

### Reviewer Steps
- [ ] Verify Bug exists
- [ ] Decide Action
  - [ ] Code change - Open MR
  - [ ] Configuration Change
    - [ ] Code - Open MR
    - [ ] Deploy
    - [ ] Server
  - [ ] User Error
    - [ ] Process Change Needed?
- [ ] Time Estimate

## Steps to Reproduce

## Actual Result
<Behaviour>

## Expected Result
<Behaviour>

## Logs / Screenshots

### Notify
<Who else should be aware of this bug>

### Related Issues

### Build Ref
<link Git SHA or pipeline build no>

/label ~Niva-AI ~Bug ~medium ~"To Do"
