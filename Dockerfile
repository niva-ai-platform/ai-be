# syntax=docker/dockerfile:1

FROM node:18.5.0
ENV NODE_ENV=dev

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install 

COPY . .
RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]